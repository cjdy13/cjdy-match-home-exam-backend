<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterUserRequest;
use App\Http\Requests\LoginUserRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    protected function register(RegisterUserRequest $request)
    {
        $validated = $request->validated();
        if($validated){
            return User::create([
                'user_name' => $request->user_name,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'password' => hash::make($request->password),
            ]);
        }
    }

    protected function login(LoginUserRequest $request)
    {
        $validated = $request->validated();
        if($validated){
            $user = DB::table('users')
                ->where('user_name', '=', $request->user_name)
                ->get();
            if($user->count() > 0){
                if (Hash::check($request->password, $user[0]->password)) {
                    return response(array('message' => 'Successfully Logged In', 'code' => 200), 200);
                } else {
                    return response(array('message' => 'Wrong Username or Password!', 'code' => 200), 200);
                }
            } else {
                return response(array('message' => 'User Does Not Exist!', 'code' => 200), 200);
            }
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      
    }
}
